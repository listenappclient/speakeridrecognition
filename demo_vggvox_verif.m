function demo_vggvox_verif(varargin)
% DEMO_VGGVOX_VERIF - minimal demo with the VGGVox model pretrained on the
% VoxCeleb dataset for Speaker Verification

% downloads the VGGVox model and
% prints the distance on a test evalutation pair

opts.modelPath = '' ;
opts.gpu = 3;
opts.dataDir = 'testfiles/verif'; 
opts = vl_argparse(opts, varargin) ;

% Example speech segments for input
inpPath1 = fullfile(opts.dataDir, '8jEAjG6SegY_0000008.wav');
inpPath2 = fullfile(opts.dataDir, 'x6uYqmx31kE_0000001.wav'); 

% Load or download the VGGVox model for Verification
modelName = 'vggvox_ver_net.mat' ;
paths = {opts.modelPath, ...
    modelName, ...
    fullfile(vl_rootnn, 'data', 'models-import', modelName)} ;
ok = find(cellfun(@(x) exist(x, 'file'), paths), 1) ;

if isempty(ok)
    fprintf('Downloading the VGGVox model for Verification ... this may take a while\n') ;
    opts.modelPath = fullfile(vl_rootnn, 'data/models-import', modelName) ;
    mkdir(fileparts(opts.modelPath)) ; base = 'http://www.robots.ox.ac.uk' ;
    url = sprintf('%s/~vgg/data/voxceleb/models/%s', base, modelName) ;
    urlwrite(url, opts.modelPath) ;
else
    opts.modelPath = paths{ok} ;
end
opts.modelPath = 'C:\MatConvNet\matconvnet-1.0-beta25\data\models-import\vggvox_ver_net.mat';
load(opts.modelPath); net = dagnn.DagNN.loadobj(netStruct);

% Remove loss layers and add distance layer
names = {'loss'} ;
for i = 1:numel(names)
    layer = net.layers(net.getLayerIndex(names{i})) ;
    net.removeLayer(names{i}) ;
    net.renameVar(layer.outputs{1}, layer.inputs{1}, 'quiet', true) ;
end
net.addLayer('dist', dagnn.PDist('p',2), {'x1_s1', 'x1_s2'}, 'distance');

% Evaluate network on GPU and set up network to be in test
% mode
%net.move('gpu');
%net.conserveMemory = 0;
net.mode = 'test' ;

% Setup buckets
buckets.pool 	= [2 5 8 11 14 17 20 23 27 30];
buckets.width 	= [100 200 300 400 500 600 700 800 900 1000];

% Load input pair and do a forward pass
inp1 = test_getinput(inpPath1, net.meta, buckets);
inp2 = test_getinput(inpPath2, net.meta, buckets);

s1 = size(inp1,2);
s2 = size(inp2,2);

p1 = buckets.pool(s1==buckets.width);
p2 = buckets.pool(s2==buckets.width);

net.layers(22).block.poolSize=[1 p1];
net.layers(47).block.poolSize=[1 p2];
featid = structfind(net.vars,'name','distance');
% net.eval({ 'input_b1', gpuArray(inp1) ,'input_b2', gpuArray(inp2) });
net.eval({ 'input_b1', inp1 ,'input_b2', inp2 });
dist = gather(squeeze(net.vars(featid).value));

% Print distance
fprintf('dist: %05d \n',dist); % should output a small distance if the two segments come from the same identity


function index=structfind(a,field,value)
% StructFind, Find the index of a certain string or value in a struct
%
%       index=structfind(a,field,value)
%
%  inputs,
%       a : A Matlab struct, for example a(1).name='red', a(2).name='blue';
%       field : The name of the field which is searched, for example 'name'
%       value : The search value, for example 'blue'
%
%  outputs,
%       index : The Struct index which match the search
%
%
% Example,
%
% a(1).name='blue';
% a(1).index=0;
% a(1).val='g';
%
% a(2).name='red';
% a(2).index=1;
% a(2).val=[1 0];
%
% a(3).name='green';
% a(3).index=2;
% a(3).val='g';
%
% a(4).name='black';
% a(4).index=3;
% a(4).val=[0 0 0];
%
% a(5).name='yellow';
% a(5).index=NaN;
% a(5).val=[0 1 1];
%
% a(6).name='orange';
% a(6).index=[];
% a(6).val=[1 1 0];
%
% a(7).name='brown';
% a(7).index=6;
% a(7).val={'12'};
%
% a(8).name='white';
% a(8).index=7;
% a(8).val.x=1;
%
% a(8).name='purple';
% a(8).index=8;
% a(8).val.child.value=2;
%
% index=structfind(a,'name','red');
% disp(['index : ' num2str(index)])
%
% index=structfind(a,'index',1);
% disp(['index : ' num2str(index)])
%
% index=structfind(a,'val',[0 0 0]);
% disp(['index : ' num2str(index)])
%
% index=structfind(a,'val','g');
% disp(['index : ' num2str(index)])
%
% index=structfind(a,'index',NaN);
% disp(['index : ' num2str(index)])
%
% index=structfind(a,'index',[]);
% disp(['index : ' num2str(index)])
%
% index=structfind(a,'val',{'12'});
% disp(['index : ' num2str(index)])
%
% index=structfind(a,'val.child.value',2);
% disp(['index : ' num2str(index)])
%
% Function is written by D.Kroon University of Twente (December 2010)
% We don't compare structs
if(isstruct(value)), 
    error('structfind:inputs','search value can not be a struct');
end
% Stop if field doesn't exist
if(~isfield(a,field))
    index=find(arrayfun(@(x)(cmp(x,field,value)),a,'uniformoutput',true));
else
    index=find(arrayfun(@(x)(cmp(x,field,value)),a,'uniformoutput',true));
end
function check=cmp(x,field,value)
check=false;
if(isfield(x,field))
    % Simple field like x.tag
    x=x.(field); 
else
    % Complex field like x.tag.child.value
    in=find(field=='.');
    s=[1 in+1]; e=[in-1 length(field)];
    for i=1:length(s)
        fieldt=field(s(i):e(i));
        if(isfield(x,fieldt)), x=x.(fieldt);  else return; end
    end
end
% We don't compare structs
if(isstruct(x)), return; end
% Values can only be equal, if they equal in length
if(length(x)==length(value)), 
    % This part compares the NaN values 
    if((~iscell(x))&&(~iscell(value))&&any(isnan(value))), 
        checkv=isnan(value); checkx=isnan(x);
        if(~all(checkx==checkv)), return; end
        x(checkx)=0; value(checkv)=0;
    end
    % This part compares for both string as numerical values 
    if(iscell(x)||iscell(value))
        check=all(strcmp(x,value)); 
    else
        check=all(x==value); 
    end
end
